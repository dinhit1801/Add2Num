﻿using System;
using System.Numerics;

namespace MyBigNumber
{
    public class MyBigNumber
    {

        public static String sum(String Num1 , String Num2)
        {
            string firstNum, secondNum;
            char[] firstArr = Num1.ToCharArray();
            char[] secondArr = Num2.ToCharArray();
            firstNum = new string(firstArr);
            secondNum = new string(secondArr);
            BigInteger res = BigInteger.Parse(firstNum) + BigInteger.Parse(secondNum);
            Console.WriteLine(res.ToString());
            return res.ToString();
        }
    }
   
}
